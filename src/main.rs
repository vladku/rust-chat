use std::thread;
use std::net::{TcpListener, TcpStream};
use std::io::{self, ErrorKind, Read, Write};
use std::time::Duration;
use std::rc::Rc;
use std::sync::{Arc, Mutex};
use std::sync::mpsc::{self, TryRecvError};
use structopt;
use structopt::StructOpt;
use structopt::clap::arg_enum;
use console::{style, Term};
use dialoguer::{theme::ColorfulTheme, Password, Input};
use serde::{Deserialize, Serialize};
use serde_json;

use std::io::{stdin, stdout};
use termion::event::Key;
use termion::input::TermRead;
use termion::raw::IntoRawMode;

#[derive(Clone)]
struct TextViewer {
    cur_pos: Coordinates,
    terminal_size: Coordinates,
    chat: String,
    user: String,
    message: String,
    messages: Vec<String>
}

impl TextViewer {
    fn init(chat: String, user: String) -> Self {
        let size = termion::terminal_size().unwrap();
        Self {
            chat: chat,
            user: user,
            cur_pos: Coordinates {
                x: 11,
                y: (size.1 - 2) as usize,
            },
            terminal_size: Coordinates {
                x: size.0 as usize,
                y: size.1 as usize,
            },
            message: String::new(),
            messages: Vec::new()
        }
    }
    fn cursor_to_pos(&mut self) {
        let x = self.cur_pos.x as u16;
        let y = self.cur_pos.y as u16;
        println!("{}", termion::cursor::Goto(x, y));
    }
    fn set_pos(&mut self, x: usize, y: usize) {
        self.cur_pos.x = x;
        self.cur_pos.y = y;
        self.cursor_to_pos();
    }

    fn show_document(&mut self) {
        let pos = &self.cur_pos;
        print!("{}{}", termion::clear::All, termion::cursor::Goto(1, 1));
        println!("{}", style(format!("Chat {}\r", self.chat))
                        .magenta().bg(console::Color::Green));
        if self.messages.len() < self.terminal_size.y {
            for line in 0..self.messages.len() {
                println!("{}\r", self.messages[line as usize]);
            }
        } else {
            if pos.y <= self.terminal_size.y {
                for line in 0..self.terminal_size.y - 3 {
                    println!("{}\r", self.messages[line as usize]);
                }
            } else {
                for line in pos.y - (self.terminal_size.y - 3)..pos.y {
                    println!("{}\r", self.messages[line as usize]);
                }
            }
        }
        println!("{}", termion::cursor::Goto(0, (self.terminal_size.y - 2) as u16));
        println!("{} {}", style("Message: ").red(), self.message);
        self.set_pos(pos.x, self.terminal_size.y - 2);
    }
    fn inc_x(&mut self) {
        if self.cur_pos.x < self.terminal_size.x && self.cur_pos.x - 11 < self.message.len() {
            self.cur_pos.x += 1;
        }
        self.cursor_to_pos();
    }
    fn dec_x(&mut self) {
        if self.cur_pos.x > 11 {
            self.cur_pos.x -= 1;
        }
        self.cursor_to_pos();
    }

    fn add_to_message(&mut self, c: char) {
        if self.cur_pos.x > 10 {
            if self.message.is_empty(){
                self.message.push(c);
            } else {
                self.message.insert(self.cur_pos.x - 11, c);
            }
            self.inc_x();
        }
        self.show_document();
    }
    fn remove_from_message(&mut self) {
        let l = self.cur_pos.x;
        let l = if l > 12 {l - 12} 
            else if l == 12 { 0 } 
            else { self.message.len() };
        if !self.message.is_empty() && l < self.message.len() {
            self.dec_x();
            self.message.remove( l);
        }
        self.show_document();
    }
}

fn show_document(pos: &Coordinates, chat: String, messages: Vec<String>, message: String, t_y: usize) {
    print!("{}{}", termion::clear::All, termion::cursor::Goto(1, 1));
    println!("{}", style(format!("Chat {}\r", chat))
                    .magenta().bg(console::Color::Green));
    if messages.len() < t_y {
        for line in 0..messages.len() {
            println!("{}\r", messages[line as usize]);
        }
    } else {
        if pos.y <= t_y {
            for line in 0..t_y - 3 {
                println!("{}\r", messages[line as usize]);
            }
        } else {
            for line in pos.y - (t_y - 3)..pos.y {
                println!("{}\r", messages[line as usize]);
            }
        }
    }
    println!("{}", termion::cursor::Goto(0, (t_y - 2) as u16));
    println!("{} {}", style("Message: ").red(), message);
    println!("{}", termion::cursor::Goto(pos.x as u16, (t_y - 2) as u16));
}

fn run(this: Arc<Mutex<TextViewer>>, tx: mpsc::Sender<String>) {
    let mut stdout = stdout().into_raw_mode().unwrap();
    let stdin = stdin();
    {
        let mut _self = this.lock().unwrap();
        _self.show_document();
    }
    //self.cursor_to_pos();
    for c in stdin.keys() {
        let mut _self = this.lock().unwrap();
        match c.unwrap() {
            Key::Esc => {
                print!("{}{}", termion::clear::All, termion::cursor::Goto(1, 1));
                break;
            }
            Key::Left => { _self.dec_x(); }
            Key::Right => { _self.inc_x(); }
            Key::Backspace => { _self.remove_from_message(); }
            Key::Char(c) => { 
                if c == '\n' {
                    let msg = Message{ 
                        user: _self.user.to_string(), 
                        text: _self.message.to_string()
                    };
                    let msg = serde_json::to_string(&msg)
                        .expect("failed convert to json");
                    tx.send(msg).unwrap();
                    _self.message = "".to_string();
                    _self.cur_pos.x = 11;
                    _self.show_document();
                } else {
                    _self.add_to_message(c); 
                }
            }
            _ => {}
        }
        stdout.flush().unwrap();
    }
}

#[derive(Debug, Copy, Clone)]
struct Coordinates {
    pub x: usize,
    pub y: usize,
}

const LOCAL: &str = "127.0.0.1:3333";

fn sleep() {
    thread::sleep(::std::time::Duration::from_millis(100));
}

fn server() {
    let server = TcpListener::bind(LOCAL).expect("Listener failed to bind");
    server.set_nonblocking(true).expect("failed to initialize non-blocking");

    let mut clients = vec![];
    let (tx, rx) = mpsc::channel::<String>();
    println!("Server started!");
    loop {
        if let Ok((mut socket, addr)) = server.accept() {
            println!("Client {} connected", addr);

            let tx = tx.clone();
            clients.push(socket.try_clone().expect("failed to clone client"));

            thread::spawn(move || loop {
                let mut buf = String::new();
                match socket.read_to_string(&mut buf){
                    Ok(_) => {}, 
                    Err(ref err) if err.kind() == ErrorKind::WouldBlock => (),
                    Err(_) => {
                        println!("closing connection with: {}", addr);
                        break;
                    }
                }
                if !buf.is_empty() {
                    let msg: Message = serde_json::from_str(&buf).expect("failed to parse json");
                    println!("{}: {:?}", addr, msg);
                    let msg = serde_json::to_string(&msg).expect("failed to str");
                    tx.send(msg).expect("failed to send msg to rx");
                }

                sleep();
            });
        }

        if let Ok(msg) = rx.try_recv() {
            clients = clients.into_iter().filter_map(|mut client| {
                let buff = msg.clone().into_bytes();
                client.write_all(&buff).map(|_| client).ok()
            }).collect::<Vec<_>>();
        }

        sleep();
    }
}

#[derive(Debug, Deserialize, Serialize)]
struct Message {
    user: String,
    text: String,
}

fn client() {
    let mut client = TcpStream::connect(LOCAL).expect("Stream failed to connect");
    client.set_nonblocking(true).expect("failed to initiate non-blocking");

    let (tx, rx) = mpsc::channel::<String>();

    let user_name = Input::<String>::new()
        .with_prompt("Enter you name")
        .interact_text().unwrap();

    println!("{}", termion::clear::All);
    println!("{}", termion::cursor::Show);
    println!("{}", termion::cursor::Goto(1, 1));
    // Initialize editor
    let editor = TextViewer::init(String::from("Test"), user_name);
    let editor = Arc::new(Mutex::new(editor));

    thread::spawn({
        let editor_clone = Arc::clone(&editor);
        move || loop {
            let mut buf = String::new();
            match client.read_to_string(&mut buf) {
                Ok(_) => {},
                Err(ref err) if err.kind() == ErrorKind::WouldBlock => (),
                Err(_) => {
                    println!("connection with server was severed");
                    break;
                }
            }
            if !buf.is_empty() {
                let msg: Message = serde_json::from_str(&buf).expect("failed to parse json");
                let mut editor = editor_clone.lock().unwrap();
                editor.messages.push(format!("{}: {}", style(msg.user.clone()).cyan(), msg.text));
                editor.show_document();
            }

            match rx.try_recv() {
                Ok(msg) => {
                    let buff = msg.clone().into_bytes();
                    client.write_all(&buff).unwrap();
                }, 
                Err(TryRecvError::Empty) => (),
                Err(TryRecvError::Disconnected) => break
            }

            thread::sleep(Duration::from_millis(100));
    }});

    run(editor, tx);
}

arg_enum! {
    #[derive(Debug)]
    enum SocketType {
        Client,
        Server
    }
}

/// Search for a pattern in a file and display the lines that contain it.
#[derive(Debug, StructOpt)]
#[structopt(name = "example", about = "An example of StructOpt usage.")]
struct Cli {
    /// The pattern to look for
    #[structopt(possible_values = &SocketType::variants(), case_insensitive = true)]
    socket_type: SocketType
}


fn main() {
    let args = Cli::from_args();
    match args.socket_type {
        SocketType::Client => client(),
        SocketType::Server => server(),
    }
}
